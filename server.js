const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const appConfig = require("./app/config/app.config");
const dbConfig = require("./app/config/db.config");

const app = express();

const whitelist = appConfig.corsWhitelist;
const corsOptions = {
  origin: whitelist,
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");

db.mongoose
  .connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
  })
  .catch((err) => {
    console.error("Connection error", err);
    process.exit();
  });

// simple route
app.get("/", (req, res) => {
  res.json({ status: "on" });
});

// routes
require("./app/routes/user.routes")(app);
require("./app/routes/serviceRequest.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
