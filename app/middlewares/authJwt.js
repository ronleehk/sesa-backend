const jwt = require("jsonwebtoken");
const appConfig = require("../config/app.config.js");
const db = require("../models");
const User = db.user;

verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];
  if (!token) {
    return res.status(403).send({ message: "No token provided!" });
  }

  jwt.verify(token, appConfig.secret, async (err, decoded) => {
    if (err) {
      console.log(err);
      return res.status(401).send({ message: "Unauthorized!" });
    }
    req.userEmail = decoded.id;
    user = User.findOne({ email: req.userEmail })
      .then((user) => {
        req.user = user;
        req.userId = user.id;
        next();
      })
      .catch((e) => {
        console.log(e);
        return res.status(401).send({ message: "Unauthorized!" });
      });
  });
};

const authJwt = {
  verifyToken,
};
module.exports = authJwt;
