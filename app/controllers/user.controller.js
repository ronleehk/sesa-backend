var mongoose = require("mongoose");
const appConfig = require("../config/app.config");
const mailer = require("../services/mailer");
const db = require("../models");
const User = db.user;
const ServiceRequest = db.serviceRequest;

const jwt = require("jsonwebtoken");

exports.signin = async (req, res) => {
  try {
    let [username, domain] = req.body.email.split("@");
    if (domain !== appConfig.allowedDomain) {
      throw new Error("invalid domain");
    }
    let user = await User.findOne({ email: req.body.email });
    const token = jwt.sign({ id: req.body.email }, appConfig.secret, {
      expiresIn: appConfig.tokenLife,
    });
    if (!user) {
      user = new User({
        username,
        email: req.body.email,
        token,
        credit: appConfig.defaultCredit, // Initial credits
      });
      await user.save();
    }

    let mailOptions = {
      to: req.body.email,
      subject: `Sesa Login Request`,
      text: `Please login with ${appConfig.host}/signin?access_token=${token}`,
    };
    mailer.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
      }
    });

    res.status(200).send({ message: true });
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: err });
  }
};

exports.verify = async (req, res) => {
  try {
    res.status(200).send({
      id: req.user.id,
      username: req.user.username,
      email: req.user.email,
      accessToken: req.user.token,
      credit: req.user.credit,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: err });
  }
};

exports.signout = async (req, res) => {
  req.user.token = undefined;
  try {
    await req.user.save();
    res.status(200).send({ message: true });
  } catch (err) {
    res.status(500).send({ message: err });
  }
};

exports.getMe = async (req, res) => {
  let user = await User.findOne({ email: req.body.email });
  if (!user) {
    return res.status(404).send({ message: "User not found" });
  }
  const now = new Date();
  const start = new Date(
    now.getFullYear(),
    now.getMonth(),
    now.getDate() - 7,
    1,
    0,
    0
  );
  const requests = await ServiceRequest.aggregate([
    {
      $match: {
        to: req.userId,
        createdAt: {
          $gt: start,
        },
      },
    },
    {
      $group: {
        _id: { $dateToString: { format: "%Y-%m-%d", date: "$doneAt" } },
        total: {
          $sum: "$credit",
        },
      },
    },
  ]);
  res.status(200).send({ requests });
};
