const { default: ShortUniqueId } = require("short-unique-id");
const uid = new ShortUniqueId();

const appConfig = require("../config/app.config");
const slack = require("../services/slack");
const mailer = require("../services/mailer");
const db = require("../models");
const ServiceRequest = db.serviceRequest;

exports.createServiceRequest = async (req, res) => {
  try {
    const serviceType = appConfig.serviceTypes.find(
      (p) => p.type == req.body.request.type && p.cat == req.body.request.cat
    );

    if (serviceType.credit > req.user.credit) {
      throw new Error("not enough credit");
    }
    req.user.credit -= serviceType.credit;
    await req.user.save();

    const serviceRequest = new ServiceRequest({
      cat: serviceType.cat,
      type: serviceType.type,
      credit: serviceType.credit,
      from: req.userId,
      code: uid(),
      status: "pending",
      side: req.body.request.side,
    });
    await serviceRequest.save();

    const subject = `Sesa! ${serviceRequest.side.toUpperCase()} side pls - $${
      serviceType.credit
    } `;
    const message = `Click this to help and redeem sesacash ${appConfig.host}/accept/${serviceRequest.code}`;

    if (serviceType.email) {
      let mailOptions = {
        to: "ronlee@palo-it.com",
        subject,
        text: message,
      };
      mailer.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        }
      });
    }

    if (serviceType.slack) {
      slack
        .send(subject + message)
        .then()
        .catch();
    }
    res.status(200).send({ message: true });
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: err });
  }
};

exports.getServiceRequests = async (req, res) => {
  const query =
    req.query && req.query.to == "1"
      ? { to: req.userId }
      : { from: req.userId };
  let requests = await ServiceRequest.find(query)
    .populate({ path: "to", select: "id username" })
    .populate({ path: "from", select: "id username" })
    .limit(req.query.limit || 10)
    .sort({ createdAt: -1 });
  res.status(200).send({ requests });
};

exports.updateServiceRequest = async (req, res) => {
  const serviceRequest = await ServiceRequest.findOne({
    code: req.params.code,
  });
  if (!serviceRequest) {
    res.status(404).send({ message: "Service request not found" });
    return;
  } else if (serviceRequest.status == "accepted") {
    res
      .status(404)
      .send({ message: "Service request has already been accepted by others" });
    return;
  } else if (serviceRequest.status == "expired") {
    res.status(404).send({ message: "Service request expired" });
    return;
  }
  const serviceType = appConfig.serviceTypes.find(
    (p) => p.type == serviceRequest.type
  );
  serviceRequest.status = "accepted";
  serviceRequest.to = req.userId;
  serviceRequest.doneAt = Date.now();
  req.user.credit += serviceType.credit;
  await req.user.save();
  await serviceRequest.save();
  res.status(200).send({ serviceRequest, credit: req.user.credit });
};
