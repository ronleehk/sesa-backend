const mongoose = require("mongoose");

const User = mongoose.model(
  "User",
  new mongoose.Schema({
    avatar: String,
    username: String,
    email: String,
    phone: String,
    token: String,
    credit: Number,
  })
);

module.exports = User;
