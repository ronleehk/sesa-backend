const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ServiceRequest = mongoose.model(
  "ServiceRequest",
  new mongoose.Schema(
    {
      cat: String,
      type: String,
      to: { type: Schema.Types.ObjectId, ref: "User" },
      from: { type: Schema.Types.ObjectId, ref: "User" },
      code: String,
      credit: Number,
      side: {
        type: String,
        enum: ["main", "restroom", "cyan", "pantry"],
      },
      extra: Object,
      status: {
        type: String,
        enum: ["pending", "accepted", "expired"],
      },
      doneAt: Date,
    },
    { timestamps: true }
  )
);

module.exports = ServiceRequest;
