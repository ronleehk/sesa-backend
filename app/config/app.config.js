module.exports = {
  corsWhitelist: [
    "http://localhost:8080",
    "http://localhost:3000",
    "https://heysesa.netlify.app",
  ],
  host: "https://heysesa.netlify.app",
  emailSender: "sesa.paloit@gmail.com",
  emailPassword: "Heysesa111",
  secret: "sesa-secret-key",
  tokenLife: 30 * 86400, // 30 days
  nexmoAPIKey: "",
  nexmoAPISecret: "",
  brandName: "Sesa",
  allowedDomain: "palo-it.com",
  defaultCredit: 20,
  slackWebhook: "",
  serviceTypes: [
    { cat: "door", type: "normal", credit: 1, email: true, slack: false },
    {
      cat: "door",
      type: "urgent",
      credit: 5,
      email: true,
      slack: false,
      sms: true,
    },
  ],
};
