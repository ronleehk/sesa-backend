const { authJwt } = require("../middlewares");
const controller = require("../controllers/user.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post("/api/auth/signin", controller.signin);
  app.post("/api/auth/verify", authJwt.verifyToken, controller.verify);
  app.post("/api/auth/signout", authJwt.verifyToken, controller.signout);
  app.get("/api/me", controller.getMe);
};
