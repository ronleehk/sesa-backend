const { authJwt } = require("../middlewares");
const controller = require("../controllers/serviceRequest.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  app.post(
    "/api/service-requests",
    authJwt.verifyToken,
    controller.createServiceRequest
  );
  app.put(
    "/api/service-requests/:code",
    authJwt.verifyToken,
    controller.updateServiceRequest
  );
  app.get(
    "/api/service-requests",
    authJwt.verifyToken,
    controller.getServiceRequests
  );
};
