const { IncomingWebhook } = require("@slack/webhook");
const appConfig = require("../config/app.config");
const url = appConfig.slackWebhook;

const webhook = new IncomingWebhook(url);

// Send the notification
module.exports = {
  send: async (message) => {
    await webhook.send({
      text: message,
    });
  },
};
