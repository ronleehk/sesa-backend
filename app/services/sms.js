const Nexmo = require("nexmo");
const appConfig = require("../config/app.config");

const nexmo = new Nexmo({
  apiKey: appConfig.NEXMO_API_KEY,
  apiSecret: appConfig.NEXMO_API_SECRET,
});

module.exports = {
  send: (from, to, text) => {
    nexmo.message.sendSms(from, to, text, (err, responseData) => {
      if (err) {
        console.log(err);
      } else {
        if (responseData.messages[0]["status"] === "0") {
          console.log("Message sent successfully.");
        } else {
          console.log(
            `Message failed with error: ${responseData.messages[0]["error-text"]}`
          );
        }
      }
    });
  },
};
