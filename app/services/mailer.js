const nodeMailer = require("nodemailer");
const appConfig = require("../config/app.config");

let transporter = nodeMailer.createTransport({
  service: "gmail",
  auth: {
    user: appConfig.emailSender,
    pass: appConfig.emailPassword,
  },
});

module.exports = transporter;
