# Sesa - An internal platform for service matching and reward

## Background

I used to use vue, koa (node.js), mongo, docker and ECS as my tech stack. This mini project is for me to refresh my knowledge on React, Redux, React Saga, GCP, Kubernetes, etc.

## Project setup

### Backend (sesa-app)

```
npm install
node server.js
```

Configuration
please edit the file app/config/app.config.js and app/config/db.config.js

### Frontend (sesa-frontend)

```
npm install
npm run start
```

### CI/CD

GCP Project ID probable-signal-278201

```
export PROJECT_ID=[Your project id]
```

## Deployment (Backend)

### Build the docker image for backend

```
docker build -t gcr.io/$PROJECT_ID/sesa-app:latest .
```

### Push the docker image to GCR

First, set up Docker to push to Container Registry:

```
gcloud auth configure-docker
```

```
docker push gcr.io/$PROJECT_ID/sesa-app:latest
```

### Create deployment

```
kubectl create -f sesa-app-deployment.yaml
```

### Create Service (Expose the deployment)

```
kubectl create -f sesa-app-service.yaml
```

### Create ManagedCertificate

```
kubectl create -f sesa-app-managedcert.yaml
```

### Create Ingress

```
kubectl create -f sesa-app-ingress.yaml
```

GCP Kubernetes Endpoint

## Deployment (Database)

Reference
https://docs.mongodb.com/kubernetes-operator/master/deploy/
https://medium.com/@dilipkumar/standalone-mongodb-on-kubernetes-cluster-19e7b5896b27

## Deployment (Frontend)

The frontend is hosted on Netlify, which is a free and scalable service with a lot of features (e.g. SSL)

Whenever you commit and push the latest source to master branch, the site will be automatically deployed.

You can check it on https://heysesa.netlify.app

### Running a Node.js Container in Kubernetes with Kubernetes Engine

Ref https://codelabs.developers.google.com/codelabs/cloud-running-a-nodejs-container/index.html?index=..%2F..index#0
https://circleci.com/blog/simplifying-your-ci-cd-build-pipeline-to-gke-with-circleci-orbs/

## TODO (For Learning)

- Add CircleCI, Sonarcloud for CI/CD
- Add tests
- Use Secret for k8s
- Manual update name
- Graph

## TODO (Crazy)

- Connect to Voice Assistant
- Perks
